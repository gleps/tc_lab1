%%
clear all;
Fs = 8e3;
t = 0:1/Fs:1;
t = t';

A = 2;
f0 = 1e3;
phi = pi/4;
s1 = A*cos(2*pi*f0*t + phi);
alpha = 1e3;
s2 = exp(-alpha*t).*s1;

figure;
subplot(2, 2, 1); plot(t(1:100), s2(1:100));
subplot(2, 2, 2); plot(t(1:100), s2(1:100), '.');
subplot(2, 2, 3); stem(t(1:100), s2(1:100));
subplot(2, 2, 4); stairs(t(1:100), s2(1:100));

%%
figure;
f = [600 1400];
s3 = cos(2*pi*t*f);
plot(t(1:100), s3(1:100, :));

%% 2
figure;
x = 100; % scale
Fs = 1e6;
t = 0:1/Fs:1;
s = A*exp(-alpha*t).*(t >= 0.005);

Nfft = length(s)/2;
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t(1:0.01*length(s)), s(1:0.01*length(s))); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%%
T = 0.005;

%%
figure;
s = A*(abs(t) <= T);
plot(t(1:100), s(1:100));

%%
figure;
s = A*t/T.*(t >= 0).*(t <= T);
plot(t(1:100), s(1:100));

%% Rect impulse
figure;
x = 40; % scale
Fs = 1e5
t = -40e-3:1/Fs:40e-3;
T = 20e-4;
s = rectpuls(t, T);

Nfft = length(s)/2;
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%% 3 Rect impulse pair
figure;
x = 40; % scale
Fs = 1e5;
t = -40e-3:1/Fs:40e-3;
T = 5e-3;
A = 5;
s = -A*rectpuls(t+T/2, T) + A*rectpuls(t-T/2, T);

Nfft = length(s)/2;
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%% triangle impulse
figure;
x = 800; % scale
Fs = 1e6;
t = -50e-3:1/Fs:50e-3;
T2 = 60e-4;
s = tripuls(t, T2);

Nfft = length(s);
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%% trapezoid impulse
figure;
x = 1000; % scale
Fs = 1e6;
t = -50e-3:1/Fs:50e-3;
A = 10;
T1 = 20e-4;
T2 = 60e-4;
s = A*(T2*tripuls(t, T2) - T1*tripuls(t, T1))/(T2 - T1);
plot(t, s);

Nfft = length(s);
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%% sinc
figure;
x = 400; % scale
t = -10*pi:0.001:10*pi;
s = sinc(t);

Nfft = length(s);
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');
%% radio impulse
figure;
x = 4; % scale
T = 2*pi;
t = -10*pi:0.1:10*pi;
s = rectpuls(t, T).*cos(t);

Nfft = length(s)/2;
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%% radio impulse & spectrum
figure;
x = 1; % scale
Fs = 1e3;
t = -0.1:1/Fs:0.1;
f0 = 10;
T = 1/f0;
s = rectpuls(t, T) .* cos(2*pi*f0*t);
f = -50:50;
sp = T/2 * (sinc((f-f0)*T)+sinc((f+f0)*T));
plot(t, s)
ylim([-1.1 1.1])
plot(f, abs(sp))

%%
figure;
x = 2; % scale
Fs = 16e3;
t = -10e-3:1/Fs:10e-3;
Fc = 4e3;
bw = 0.1;
bwr = -20;
s = gauspuls(t, Fc, bw, bwr);
Nfft = 2^nextpow2(length(s));
sp = fft(s, Nfft);
sp_dB = 20*log10(abs(sp));
f = (0:Nfft-1)/Nfft*Fs;
plot(t, s);
figure;
plot(f(1:Nfft/2), sp_dB(1:Nfft/2));
sp_max_db = 20*log10(max(abs(sp)));
edges = Fc*[1-bw/2 1+bw/2];

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp_dB(1:Nfft/x)); title('spectrum(dB)');


%% 4 
figure;
x = 1e4; % scale
Fs = 1e6;
t = -1:1/Fs:2;
tau = 20e-3;
d = [20 80 160 260 380]'*1e-3;
d(:,2) = 0.8.^(0:4)';
s = pulstran(t, d, 'tripuls', tau);
plot(t, s);

Nfft = length(s)/2;
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%%
x = 1e4; % scale
Fs0 = 400;
tau = 60e-3;
t0 = 0:1/Fs0:tau;
s0 = sin(pi*t0/tau).^2;
Fs = 1e6;
t = -1:1/Fs:1;
d = (1:6)'*64e-3;
d(:,2) = 0.6.^(0:5)';
s = pulstran(t, d, s0, Fs0);

Nfft = length(s);
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%% 5 

%%
x = 1000; % scale
Fs = 1e6;
t = -100e-3:1/Fs:100e-3;
A = 3;
f0 = 50;
tau = 5e-3;
s = (square(2*pi*t*f0, f0*tau*100) + 1)*A/2;

Nfft = length(s)/2;
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); plot(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');


%%
x = 10000; % scale
Fs = 1e6;
t = -400e-3:1/Fs:400e-3;
A = 5;
T = 50e-3;
t1 = 5e-3;
s = (sawtooth(2*pi*t/T, 1-t1/T)-1)*A/2;

Nfft = length(s)/2;
sp = 2*abs(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); stem(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%% 
x = 1e5; % scale
Fs = 1e5;
t = 0:1/Fs:30;
s = diric(t,7);
plot(t,diric(t,7))
grid on
title('n = 7')
figure;
plot(t, diric(t,8))
grid on
title('n = 8')

Nfft = length(s)/2;
sp = 2*(fft(s, Nfft)/Nfft);
f = (0:Nfft-1)/Nfft*Fs;

figure;
subplot(2, 1, 1); plot(t, s); title('signal');
subplot(2, 1, 2); stem(f(1:Nfft/x), sp(1:Nfft/x)); title('spectrum');

%% 6 
Fs = 8e3;
t = 0:1/Fs:1;
t1 = 1;
f0 = 1e3;
f1 = 2e3;
s1 = chirp(t, f0, t1, f1, 'linear');
s2 = chirp(t, f0, t1, f1, 'quadratic');
s3 = chirp(t, f0, t1, f1, 'logarithmic');
specgram(s1, [], Fs)
title('linear')
colormap gray
figure
specgram(s2, [], Fs)
title('quadratic')
colormap gray
figure
specgram(s3, [], Fs)
title('logarithmic')
colormap gray

%%
x = [0 0 0 1 0 1 0 1 1 1 0 0 0 0 1 0];
y = [1 0 1];

xx = x;
yy = zeros(1,length(x));
yy(y == 1) = 1;
yy(y == 0) = -1;

R = zeros(1,length(xx));

tic
for i = 1:length(xx)
    R(i) = sum(xx .* circshift(yy, i-1)) / length(xx);
end
toc

tic
xx = fft(xx);
yy = fft(yy);
xx = conj(xx);
BR = ifft(xx .* yy)/length(xx);
toc
